# public-keys

**Repository:** https://gitlab.com/jamesshulman/public-keys

**Description:** This repository has been made public and contains my **public** GPG & SSH keys used for encryption and secure communication

**Key Information:**
- <a href="jamesshulman.gpg.asc">jamesshulman.gpg.asc</a> --> My primary GPG key
  - SHA1: <a href="jamesshulman.gpg.asc.sha1">jamesshulman.gpg.asc.sha1</a>
  - Type: rsa4096
- <a href="jamesshulman.ed25519.pub">jamesshulman.ed25519.pub</a> --> My primary SSH (Ed25519) key
  - SHA1: <a href="jamesshulman.ed25519.pub.sha1">jamesshulman.ed25519.pub.sha1</a>
  - Type: ed25519
- <a href="jamesshulman.rsa.pub">jamesshulman.rsa.pub</a> --> My SSH (RSA) key for compatibility
  - SHA1: <a href="jamesshulman.rsa.pub.sha1">jamesshulman.rsa.pub.sha1</a>
  - Type: rsa4096

<br>
<hr>
My Personal Website: <a href="https://www.jamesshulman.com">www.jamesshulman.com</a>
<br>
For any change inquiries/suggestions, please email <a href="mailto:me@jamesshulman.com">me@jamesshulman.com</a>